package model.equipment.raw;

import java.util.ArrayList;

public class Backpack extends ArrayList<Item> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int maxSize;
	
	public Backpack(int maxSize) {
		super();
		this.maxSize = maxSize;
	}
	
	public boolean addItem(Item item){
		
		if(size() == maxSize)
			return false;
		else
		{
			add(item);
			return true;
		}
	}
}
