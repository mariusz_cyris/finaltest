package model.equipment.raw.item.type;

import model.equipment.raw.Item;
import model.equipment.raw.ItemType;
import java.util.Random;

public class Armor extends Item {

	private int armor;
	
	private int dodge;
	
	private ArmorType armorType;
	
	Random randomGenerator = new Random();

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}

	
	public int getDodge() {
		return dodge;
	}

	public void setDodge(int dodge) {
		this.dodge = dodge;
	}

	public int isAttackDodge(){
		return randomGenerator.nextInt(100);
	}
	
	
	public ArmorType getArmorType() {
		return armorType;
	}

	public void setArmorType(ArmorType armorType) {
		this.armorType = armorType;
	}

	public Armor(String name, ArmorType armorType, ItemType itemType, float value, int armor, int dodge) {
		super(name, itemType, value);
		this.armor = armor;
		this.dodge = dodge;
		this.armorType = armorType;
	}
	
}
