package model.equipment.raw.item.type;

import model.equipment.raw.Item;
import model.equipment.raw.ItemType;

public class Weapon extends Item {
	
	private int attack;
	
	private int defense;
	
	public int getAttack() {
		return attack;
	}
	
	public void setAttack(int attack) {
		this.attack = attack;
	}
	

	
	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}
	
	public Weapon(String name, ItemType itemType, float value, int attack, int defense) {
		super(name, itemType, value);
		this.attack = attack;
		this.defense = defense;
	}

}
