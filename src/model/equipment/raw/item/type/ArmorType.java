package model.equipment.raw.item.type;

public enum ArmorType {

	BUTKI,
	SKARPETKI,
	KAMIZELECZKA,
	STANICZEK,
	MAJTECZKI;
}
