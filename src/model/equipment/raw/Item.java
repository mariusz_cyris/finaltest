package model.equipment.raw;

import model.Entity;

public class Item extends Entity {

	private ItemType itemType;
	
	private float value;
	
	public ItemType getItemType() {
		return itemType;
	}
	
	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}
	
	public float getValue() { 
		return value;
	}
	
	public void setValue(float value) {
		this.value = value;
	}
	
	public Item(String name, ItemType itemType, float value) {
		super(name);
		this.itemType = itemType;
		this.value = value;
	}
}