package model.equipment.raw;

public enum Slot {
	WEAPON,
	ARMOR,
	LEFT_RING,
	RIGHT_RING,
	SHIELD;
}
