package model.equipment.raw;

public enum ItemType {
	WEAPON,
	ARMOR,
	RING,
	SHIELD;
}
