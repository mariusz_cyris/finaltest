package model.equipment;

import model.equipment.raw.Backpack;
import model.equipment.raw.EquipmentSlots;

public class PlayerEquipment {

	private Backpack backpack;
	
	private EquipmentSlots equipment;
	
	public Backpack getBackpack() {
		return backpack;
	}
	
	public void setBackpack(Backpack backpack) {
		this.backpack = backpack;
	}
	
	public EquipmentSlots getEquipment() {
		return equipment;
	}
	
	public PlayerEquipment() {
		super();
		this.backpack = new Backpack(999);
		this.equipment = new EquipmentSlots();
	}

	public void setEquipment(EquipmentSlots equipment) {
		this.equipment = equipment;
	}
}
