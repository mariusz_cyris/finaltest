package model.creature.human;

import model.equipment.PlayerEquipment;

public class Player extends Human {

	private float experience;
	
	private int level;
	
	private PlayerEquipment equipment;
	
	public PlayerEquipment getEquipment(){
		return equipment;
	}
	
	public void setEquipment(PlayerEquipment equipment) {
		this.equipment = equipment;
	}
	
	public float getExperience() {
		return experience;
	}
	
	public void setExperience(float experience) {
		this.experience = experience;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}
	
	public Player(String name, int healthPoints, int manaPoints, float experience, int level,
			PlayerEquipment equipment){
		super(name, healthPoints, manaPoints);
		this.experience = experience;
		this.level = level;
		this.equipment = equipment;
	}
}
