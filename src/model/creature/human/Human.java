package model.creature.human;

import model.creature.Creature;

public class Human extends Creature {

	private int manaPoints;
	
	public Human(String name, int healthPoints, int manaPoints) {
		super(name, healthPoints);
		this.manaPoints = manaPoints;
	}
	
	public int getManaPoints() {
		return manaPoints;
	}
	
	public void setManaPoints(int manaPoints) {
		this.manaPoints = manaPoints;
	}
}
