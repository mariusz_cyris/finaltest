package model.creature.monsters;

import org.apache.commons.lang3.RandomStringUtils;

import model.creature.Creature;
import model.equipment.raw.Backpack;

public class Monster extends Creature {
	
	private int expValue;
	
	private Backpack loot;
	
	private MonsterType monsterType;
	
	public MonsterType getMonsterType() {
		return monsterType;
	}
	
	public void setMonsterType(MonsterType monsterType) {
		this.monsterType = monsterType;
	}
	
	public int getExpValue() {
		return expValue;
	}
	
	public void setExpValue(int expValue){
		this.expValue = expValue;
	}
	
	public Backpack getLoot() {
		return loot;
	}
	
	public void setLoot(Backpack loot) {
		this.loot = loot;
	}
	
	public Monster(String name, int healthPoints, int expValue, Backpack loot,
			MonsterType monsterType) {
		super(name, healthPoints);
		this.expValue = expValue;
		this.loot = loot;
		this.monsterType = monsterType;
	}
	
	public Monster(String name, int healthPoints) {
		super(name, healthPoints);
	}

	public Monster(int healthPoints, int expValue, Backpack loot, MonsterType monsterType){
		super(RandomStringUtils.randomAlphabetic(5) + " "+monsterType.getName(), healthPoints);
		this.expValue = expValue;
		this.loot = loot;
		this.monsterType = monsterType;
	}
	
	
}
