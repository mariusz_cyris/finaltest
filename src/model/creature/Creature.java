package model.creature;

import model.Entity;

public class Creature extends Entity {
	
	private int healthPoints;
	private int damagePoints;
	
	public int getHealthPoints() {
		return healthPoints;
	}
	
	public void setHealthPoints(int healthPoints) {
		this.healthPoints = healthPoints;
	}
	
	public int getDamagePoints() {
		return damagePoints;
	}
	
	public void setDamagePoints(int damagePoints)
	{
		this.damagePoints = damagePoints;
	}
	
	public Creature(String name, int healthPoints){
		super(name);
		this.healthPoints = healthPoints;
	}
	
}
