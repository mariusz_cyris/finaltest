package server;

import java.util.Scanner;

import consts.UtilityConsts;
import model.creature.human.Player;
import model.creature.monsters.Monster;
import model.equipment.PlayerEquipment;
import server.helpers.Menu;
import services.LifeStatus;
import services.MonsterServicesImp;
import services.PlayerServicesImp;

public class GameImpl implements Game{

	private MonsterServicesImp monsterService = new MonsterServicesImp();
	private PlayerServicesImp playerServicesImp = new PlayerServicesImp();
	private Player player = new Player("Mariuszek gruby brzuszek", 100, 100, 0.0f, 1, new PlayerEquipment());
	
	@Override
	public void run() {
		
		player.setDamagePoints(3);
        Scanner scanner = new Scanner(System.in);
        
		while(true)
		{
			Menu.showMenu();
			String choice = scanner.nextLine();
			
			switch(choice)
			{
				case "1":
				{
					Monster monster = monsterService.generateRandomMonster();
					monsterHasAppered(monster);
					
					while(true)
					{
						showHealths(monster);
						if(playerServicesImp.attack(player, monster) == LifeStatus.DEAD)
						{
							System.out.println("Zabiles potwora Ty kurwo");
							break;
						}
						// Tutaj dodać atak potwora
					}
					
					break;
					
				}
				/*
				 * Tutaj dodać zakładanie ekwipunku
				 * Np. na początku wylistuj przedmioty w bp wraz z ich numerkiem (pozycja w arrayliscie)
				 * i potem jakies opcje typu zaloz, nastepnie wybierasz nr itemu z backpacka i patrzac na jakie Slot
				 * zamienia z aktualnie zalozonym a tamten daje do backpacka. Powodzonka! 
				 */
				case "5":
				{
					System.out.println(UtilityConsts.END_OF_GAME_MESSAGE);
					System.exit(0);
					break;
				}
			}
			
		}
		
	}
	
	
	private void monsterHasAppered(Monster monster)
	{
		System.out.println(UtilityConsts.MONSTER_APPEARED + ": " + monster.getMonsterType().getName() + " - " + monster.getName());
	}
	
	private void showHealths(Monster monster)
	{
		System.out.println("Monster: " + monster.getHealthPoints() + ", Player: " + player.getHealthPoints());
	}

}
