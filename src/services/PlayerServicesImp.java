package services;

import model.creature.human.Player;
import model.creature.monsters.Monster;
import model.equipment.raw.Slot;
import model.equipment.raw.item.type.Weapon;

public class PlayerServicesImp implements CreatureServices {

	@Override
	public LifeStatus attack(Player player, Monster monster) {
		Weapon weapon = (Weapon)player.getEquipment().getEquipment().get(Slot.WEAPON);
		
		if(weapon != null)
			monster.setHealthPoints(monster.getHealthPoints()-(weapon.getAttack()+player.getDamagePoints()));
		else
			monster.setHealthPoints(monster.getHealthPoints()-(player.getDamagePoints()));
		
		if(monster.getHealthPoints() > 0){
			return LifeStatus.ALIVE;
		}
		else
		{
			player.setExperience(player.getExperience()+monster.getExpValue());
			player.getEquipment().getBackpack().addAll(monster.getLoot());
			return LifeStatus.DEAD;
		}
	}

}
