package services;

import org.apache.commons.lang3.RandomStringUtils;

import model.creature.human.Player;
import model.creature.monsters.Monster;
import model.creature.monsters.MonsterType;
import model.equipment.raw.Backpack;
import model.equipment.raw.ItemType;
import model.equipment.raw.item.type.Armor;
import model.equipment.raw.item.type.ArmorType;
import model.equipment.raw.item.type.Weapon;

public class MonsterServicesImp implements CreatureServices {

	@Override
	public LifeStatus attack(Player player, Monster monster) {

		return null;
	}
	
	public Monster generateRandomMonster()
	{
		Monster monster = new Monster(RandomStringUtils.randomAlphabetic(5),
				Integer.parseInt(RandomStringUtils.randomNumeric(2)));
		
		monster.setDamagePoints(Integer.parseInt(RandomStringUtils.randomNumeric(1)));
		monster.setExpValue(10);
		
		Backpack backpack = new Backpack(2);
		backpack.add(new Weapon("xxx", ItemType.WEAPON, 3.5f, 5, 5));
		backpack.add(new Armor("armor", ArmorType.KAMIZELECZKA, ItemType.ARMOR, 55.0f, 4, 7));
		monster.setLoot(backpack);
		
		monster.setMonsterType(MonsterType.ANDRZEJ);
		
		return monster;
	}
			
}

