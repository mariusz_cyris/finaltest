package services;

public enum LifeStatus {
	ALIVE,
	DEAD;
}