package services;

import model.creature.human.Player;
import model.creature.monsters.Monster;

public interface CreatureServices {

	public LifeStatus attack(Player player, Monster monster);
}
